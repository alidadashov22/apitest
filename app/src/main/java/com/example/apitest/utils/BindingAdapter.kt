package com.example.apitest.utils

import android.view.View
import android.widget.ImageView
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.example.apitest.R
import com.example.apitest.ui.fragments.BlankFragment
import kotlinx.android.synthetic.main.list_item.view.*

@BindingAdapter("imageUrl")
fun setImage(image: ImageView, url: String?) {

    if (!url.isNullOrEmpty()){

        Glide.with(image.context)
            .load(url).centerCrop()
            .into(image)
    }

}


