package com.example.apitest.utils

import android.view.View
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter

@BindingAdapter("uiStateLoading")
fun View.setUiStateForLoading(uiState: UiState?) {
    uiState?.let {
        this.isVisible = when (uiState) {
            Loading -> true
            else -> false
        }
    }
}