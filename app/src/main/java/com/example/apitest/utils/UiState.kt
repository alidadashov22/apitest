package com.example.apitest.utils

sealed class UiState

object Loading : UiState()
object HasData : UiState()
object NoData : UiState()