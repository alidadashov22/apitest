package com.example.apitest.utils


class RecyclerViewItemClickListener<T>(val itemClickListener: (data: T?)-> Unit, val itemClickListener1: (data: T?)-> Unit) {
    fun onItemClick(data: T) = itemClickListener(data)
    fun onItemClick1(data: T) = itemClickListener1(data)

}