package com.example.apitest.repository

import com.example.apitest.network.ApiInitHelper
import com.example.apitest.network.NetworkState
import com.example.apitest.utils.checkNetworkRequestResponse
import java.io.IOException


private val exchangeRateService = ApiInitHelper.exchangeService

suspend fun fetchRates(): NetworkState = try {

    val response = exchangeRateService.getApi()

    checkNetworkRequestResponse(response)



}catch (e: IOException) {
    NetworkState.NetworkException(e.message)
}