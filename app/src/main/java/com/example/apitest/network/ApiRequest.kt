package com.example.apitest.network

import com.example.apitest.data_model.JasonResponse
import retrofit2.Response
import retrofit2.http.GET

interface ApiRequest {


    @GET("/v1/latest-news?language=it&apiKey=7goxs3CKQUAJRGhquf6BovIkIToi_vj5Uii2lTrQCVE9EtY2")

    suspend fun getApi() : Response<JasonResponse>


}

