package com.example.apitest.ui.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.apitest.databinding.FragmentBlankBinding
import com.example.apitest.utils.RecyclerViewItemClickListener
import com.example.apitest.view_model.ViewModel
import kotlinx.android.synthetic.main.fragment_blank.*


class BlankFragment : Fragment() {
    private val viewModel2: ViewModel by viewModels()
    lateinit var adapter: RcyclerAdapter
    lateinit var binding: FragmentBlankBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentBlankBinding.inflate(inflater)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindUi()

        initRecyclerView()

        observeValue()

    }

    private fun bindUi(): Unit = with(binding) {
        lifecycleOwner = this@BlankFragment
        viewModel = viewModel2
    }

    private fun initRecyclerView(): Unit = with(binding) {
        adapter = RcyclerAdapter( RecyclerViewItemClickListener(itemClickListener = {

                    if (it != null) {
            setClickListener(url = it.url)
        }

        },
        itemClickListener1 = {

            setClickListener2()


        }
            ))

        recyclerView.adapter = adapter

    }

    private fun observeValue(): Unit = with(viewModel2) {
        //observe cached data

        exchangeData.observe(viewLifecycleOwner, Observer {
            it?.let { exchangeRates ->
                adapter.submitList(exchangeRates.news)
            }
        })


    }


    fun setClickListener(url: String) {

        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(i)


    }

    fun setClickListener2(){

        findNavController().navigate(BlankFragmentDirections.actionBlankFragmentToBlankFragment2())

    }

    override fun onStart() {
        super.onStart()

        viewModel2.getExchangeRates()
    }


    }



