package com.example.apitest.ui.fragments

import androidx.recyclerview.widget.DiffUtil
import com.example.apitest.data_model.JasonResponse
import com.example.apitest.data_model.News

object DiffCallBack : DiffUtil.ItemCallback<News>() {

    override fun areItemsTheSame(
        oldItem: News,
        newItem: News
    ): Boolean = (oldItem.id == newItem.id)

    override fun areContentsTheSame(
        oldItem: News,
        newItem: News
    ): Boolean = (oldItem == newItem)

}