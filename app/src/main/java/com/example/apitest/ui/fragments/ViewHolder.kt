package com.example.apitest.ui.fragments

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.apitest.data_model.JasonResponse
import com.example.apitest.data_model.News
import com.example.apitest.databinding.ListItemBinding
import com.example.apitest.utils.RecyclerViewItemClickListener
import kotlinx.android.synthetic.main.list_item.view.*

class ViewHolder private constructor(val binding: ListItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(Item: News, recyclerViewItemClickListener: RecyclerViewItemClickListener<News>
    ): Unit = with(binding) {
        user = Item
//        root.setOnClickListener {

//        }
        root.imageView.setOnClickListener {
            recyclerViewItemClickListener.onItemClick(Item)
        }

        root.textView.setOnClickListener {
            (recyclerViewItemClickListener.onItemClick1(Item))

        }
    }

    companion object {

        fun fromParent(parent: ViewGroup): ViewHolder {

            val inflater = LayoutInflater.from(parent.context)
            val binding = ListItemBinding.inflate(inflater, parent, false)

            return ViewHolder(binding)
        }
}
}