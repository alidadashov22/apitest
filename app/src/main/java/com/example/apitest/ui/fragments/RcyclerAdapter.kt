package com.example.apitest.ui.fragments

import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.example.apitest.data_model.News
import com.example.apitest.utils.RecyclerViewItemClickListener


class RcyclerAdapter(private val itemClickListener: RecyclerViewItemClickListener<News>
    ) : ListAdapter<News, ViewHolder>(DiffCallBack) {


        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder.fromParent(parent)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bind(getItem(position), itemClickListener)

        }



    }
