package com.example.apitest.view_model

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.apitest.R
import com.example.apitest.data_model.JasonResponse
import com.example.apitest.data_model.News
import com.example.apitest.network.NetworkState
import com.example.apitest.repository.fetchRates
import com.example.apitest.utils.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ViewModel(application: Application) : AndroidViewModel(application){

    private val context = application.applicationContext


    private val _exchangeData = MutableLiveData<JasonResponse?>()
    val exchangeData: LiveData<JasonResponse?>
        get() = _exchangeData

    private val _uiState = MutableLiveData<UiState>()
    val uiState: LiveData<UiState>
        get() = _uiState


    @Suppress("UNCHECKED_CAST")
    fun getExchangeRates() = viewModelScope.launch(Dispatchers.IO) {
        if (checkConnectivity(context)) {
            withContext(Dispatchers.Main) {
                _uiState.value = Loading
            }
            when (val response = fetchRates()) {
                is NetworkState.Success<*> -> {
                    withContext(Dispatchers.Main) {
                        _uiState.value = HasData

                        // without room database just getting data from api
                        val data = response.data as JasonResponse
                        _exchangeData.postValue(data)
                    }
                }
                is NetworkState.HandledHttpError -> handleError(response.error)

                is NetworkState.UnhandledHttpError -> handleError(response.error)

                is NetworkState.NetworkException -> handleError(response.exception)
            }
        } else {
            handleError(context.getString(R.string.error_no_internet))
        }
    }

    private suspend fun handleError(message: String?) {
        withContext(Dispatchers.Main) {
            _uiState.value = NoData

            context.Toast(message)
        }
    }

}