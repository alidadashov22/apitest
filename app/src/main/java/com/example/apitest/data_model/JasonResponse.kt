package com.example.apitest.data_model

data class JasonResponse(
    val news: List<News>,
    val page: Int,
    val status: String
)